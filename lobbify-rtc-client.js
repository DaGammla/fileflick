const lobbifyRTC = function () {

    const lobbifyRTC = {
        appName: "",
        appDigest: "",
        wsRoot: "ws://localhost:8080",
        httpRoot: "http://localhost:8080",
        useSSE: false,
        useServerAuth: false,
        relayOnlyMode: false,
        ssePostBufferTime: 100,
        rtcPeerConfigOverride: {},
        visitLobby: visit,
        createLobby: create
    }

    function createPoster(path) {

        const timeout = lobbifyRTC.ssePostBufferTime
        let postData = ""
        let timeoutId = -1

        function submit() {
            const data = postData
            postData = ""
            fetch(path, { method: "POST", body: data })
        }

        function post(data, now) {
            if (postData.length > 0) {
                postData += "%%\n"
            }
            postData += data

            clearTimeout(timeoutId)
            if (now) {
                submit()
            } else {
                timeoutId = setTimeout(submit, timeout)
            }
        }


        return {
            post
        }
    }

    async function visit(lobbyId, onError) {

        const options = JSON.parse(JSON.stringify(lobbifyRTC))

        const useWebsockets = !options.useSSE

        const joinPostOptions = { method: "POST" }
        if (options.useServerAuth) {
            joinPostOptions.headers = { "Lobbify-RTC-App": await createNowDigest() }
        }

        const resp = await fetch(options.httpRoot + "/join-lobby/" + lobbyId, joinPostOptions)

        if (!resp.ok) {
            throw `${resp.status}: ${await resp.text()}`
        }

        const joinData = (await resp.json())

        const visitorId = joinData.lobbifyRTC.id
        const visitorCode = joinData.lobbifyRTC.code

        let ws = null
        let ssePoster = null
        let sse = null

        let peer = null

        function send(prefix, data) {
            const msg = `${prefix}::${data}`
            if (useWebsockets) {
                ws.send(msg)
            } else {
                ssePoster.post(msg)
            }
        }

        function onTextMessage(incoming) {
            const firstColon = incoming.indexOf(":")
            const secondColon = incoming.indexOf(":", firstColon + 1)
            const prefix = incoming.slice(0, firstColon)
            const content = incoming.slice(secondColon + 1)
            onSignalingMessage(prefix, content)
        }

        async function onSignalingMessage(prefix, data) {
            switch (prefix){
                case "offer": {
                    await peer.setRemoteDescription(JSON.parse(data))
                    
                    await peer.setLocalDescription();

                    send("answer", JSON.stringify(peer.localDescription))

                    break;
                }
                case "candidate": {

                    if (options.relayOnlyMode && !data.includes("relay")) break;

                    const cand = JSON.parse(data)
                    peer.addIceCandidate(cand)
                    break;
                }
                case "kick": {
                    onError(data)
                    close()
                    break;
                }
            }
        }

        function close() {
            if (ws != null) {
                ws.close()
            }
            if (sse != null) {
                sse.close()
            }
        }

        let connect = null

        if (useWebsockets) {
            connect = () => {
                ws = new WebSocket(options.wsRoot + `/visitor-ws/${lobbyId}/${visitorId}/${visitorCode}`);
                ws.onmessage = (ev) => {
                    onTextMessage(String(ev.data))
                }
                ws.onclose //TODO
                ws.onerror //TODO
            }
        } else {
            ssePoster = createPoster(options.httpRoot + `/visitor-msg/${lobbyId}/${visitorId}/${visitorCode}`)
            connect = () => {
                sse = new EventSource(options.httpRoot + `/visitor-sse/${lobbyId}/${visitorId}/${visitorCode}`);
                sse.onmessage = (ev) => {
                    onTextMessage(String(ev.data))
                }
                sse.onerror //TODO
            }
        }

        peer = createRTCPeer(joinData, options.rtcPeerConfigOverride)

        peer.addEventListener("icecandidate", (ev) => {
            const cand = ev.candidate
            if (cand == null) return
            if (options.relayOnlyMode && !cand.candidate.includes("relay")) return
            send("candidate", JSON.stringify(cand))
        })

        return {
            peer,
            visitorId,
            connect,
            leave: undefined //TODO
        }
    }

    async function create(onPeerCreated, onError, limit) {
        const options = JSON.parse(JSON.stringify(lobbifyRTC))

        const useWebsockets = !options.useSSE

        const createPostOptions = { method: "POST" }
        if (options.useServerAuth) {
            createPostOptions.headers = { "Lobbify-RTC-App": await createNowDigest() }
        }

        if (limit != null) {
            createPostOptions.body = String(limit)
        }

        const resp = await fetch(options.httpRoot + "/create-lobby", createPostOptions)

        if (!resp.ok) {
            throw `${resp.status}: ${await resp.text()}`
        }

        const createData = (await resp.json())

        const lobbyId = createData.lobbifyRTC.id
        const ownerCode = createData.lobbifyRTC.code

        let ws = null
        let ssePoster = null
        let sse = null
        const peers = new Map()

        function send(prefix, data, recipient) {
            if (recipient == null){
                recipient = ""
            }
            const msg = `${prefix}:${recipient}:${data}`
            
            if (useWebsockets) {
                ws.send(msg)
            } else {
                ssePoster.post(msg)
            }
        }

        function onTextMessage(incoming) {
            const firstColon = incoming.indexOf(":")
            const secondColon = incoming.indexOf(":", firstColon + 1)
            const prefix = incoming.slice(0, firstColon)
            const visitorId = incoming.slice(firstColon + 1, secondColon)
            const content = incoming.slice(secondColon + 1)
            onSignalingMessage(prefix, content, visitorId)
        }

        function createOwnerPeer(visitorId) {
            const peer = createRTCPeer(createData, options.rtcPeerConfigOverride)
            peer.addEventListener("icecandidate", (ev) => {
                const cand = ev.candidate
                if (cand == null) return
                if (options.relayOnlyMode && !cand.candidate.includes("relay")) return
                send("candidate", JSON.stringify(cand), visitorId)
            })
            peer.addEventListener("negotiationneeded", async () => {
                await peer.setLocalDescription();
                send("offer", JSON.stringify(peer.localDescription), visitorId)
            })
            peer.addEventListener("iceconnectionstatechange", () => {
                if (peer.iceConnectionState === "failed") {
                    peer.restartIce();
                }
            })
            return peer
        }

        async function onSignalingMessage(prefix, data, visitorId) {
            switch (prefix){
                case "new": {
                    
                    const peer = createOwnerPeer(visitorId)
                    peers.set(visitorId, peer)
                    
                    onPeerCreated(peer, visitorId)

                    break;
                }
                case "answer": {
                    const p = peers.get(visitorId)
                    const desc = JSON.parse(data)
                    await p.setRemoteDescription(desc)
                    break;
                }
                case "candidate": {

                    if (options.relayOnlyMode && !data.includes("relay")) break;

                    const p = peers.get(visitorId)
                    const cand = JSON.parse(data)
                    p.addIceCandidate(cand)
                    
                    break;
                }
            }
        }

        let open = null

        if (useWebsockets) {
            open = () => {
                ws = new WebSocket(options.wsRoot + `/owner-ws/${lobbyId}/${ownerCode}`);
                ws.onmessage = (ev) => {
                    onTextMessage(String(ev.data))
                }
                ws.onclose //TODO
                ws.onerror //TODO
            }
        } else {
            ssePoster = createPoster(options.httpRoot + `/owner-msg/${lobbyId}/${ownerCode}`)
            open = () => {
                sse = new EventSource(options.httpRoot + `/owner-sse/${lobbyId}/${ownerCode}`);
                sse.onmessage = (ev) => {
                    onTextMessage(String(ev.data))
                }
                sse.onerror //TODO
            }
            
        }

        function kick(visitorId, reason = "") {
            send("kick", reason, visitorId)
            const peer = peers.get(visitorId)
            if (peer != null) {
                peer.close()
            }
        }

        function close() {
            if (useWebsockets) {
                ws.onclose = undefined
                ws.onerror = undefined
                ws.send("close::")
            } else {
                sse.close()
                ssePoster.post("close::", true)
            }

            for (const peer of peers.values()) {
                peer.close()
            }
        }

        //TODO Limit, Pause, Continue

        return {
            close,
            open,
            kick,
            lobbyId
        }
    }

    function createRTCPeer(config, override) {
        const completeConfig = {
            ...config,
            lobbifyRTC: undefined,
            ...override
        }
        return new RTCPeerConnection(completeConfig);
    }

    async function createNowDigest(){

        const time = Math.floor(Date.now() / 1000)
    
        const text = `${lobbifyRTC.appName}:${time}:${lobbifyRTC.serverDigest}`
    
        const encoder = new TextEncoder();
        const data = encoder.encode(text);
        
        const hashBuffer = await crypto.subtle.digest('SHA-256', data);
        
        const base64Hash = btoa(String.fromCharCode(...new Uint8Array(hashBuffer)));
        
        return `${lobbifyRTC.appName}:${time}:${base64Hash}`;
    }

    return lobbifyRTC
}()

