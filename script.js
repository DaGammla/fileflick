const data = $fuwuProxy()
data.dragging = false
data.isSending = false
data.isInTransfer = false
data.clientId = 0
data.modalType = "none"
data.offerId = ""
data.fileSize = 0
data.fileProgress = 0
data.fileName = ""

let fileToUpload = null

const chunkSize = 64 * 1000;

function showModal(){
    return data.modalType != "none"
}

function getProgressPercentage(){
    return parseInt(data.fileProgress * 100 / data.fileSize)
}

function dropFile(ev) {

    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
    data.dragging = false

    if (data.isInTransfer)
        return
  
    if (ev.dataTransfer.items) {
      // Use DataTransferItemList interface to access the file(s)
      [...ev.dataTransfer.items].forEach((item, i) => {
        // If dropped items aren't files, reject them
        if (item.kind === "file") {
          const file = item.getAsFile();
          uploadFile(file)
          return
        }
      });
    } else {
      // Use DataTransfer interface to access the file(s)
      [...ev.dataTransfer.files].forEach((file) => {
        uploadFile(file)
        return
      });
    }
}

function selectFile(ev) {
    const fileInput = $o("#file-upload")
    if (fileInput.files.length > 0){
        uploadFile(fileInput.files[0])
    }
}

function uploadFile(file){
    fileToUpload = file
    data.isSending = true
    data.isInTransfer = true
    initHost()
}

function receiveFile(){
    data.modalType = "offer-input"
    data.isSending = false
    data.isInTransfer = true
}

function submitReceive(){
    const offerId = $o("#offer-input").value
    if (offerId.length == 0)
        return
    caleJoin(offerId)
}

function download(blob){
    const a = document.createElement('a');
    a.href = URL.createObjectURL(blob);
    a.download = data.fileName;
    a.click();
}

function failed(){
    setTimeout(() => {
        if (data.isInTransfer) {
            reset()
            data.modalType = "failed"
        }
    }, 1000)
}

function finished(){
    reset()
    data.modalType = "finished"
}

function reset(){
    if (visitor != null){
        visitor.peer.close()
        visitor = null;
    }
    if (lobby != null){
        lobby.close();
        lobby = null;
    }
    clientResolve = null
    fileToUpload = null

    data.dragging = false
    data.isSending = false
    data.isInTransfer = false
    data.clientId = 0
    data.modalType = "none"
    data.offerId = ""
    data.fileSize = 0
    data.fileProgress = 0
    data.fileName = ""

    if (location.search.length > 0)
        history.replaceState({} , "", location.origin + location.pathname)
}

function makeQr(el){
    new QRCode(el, {
        text: getQrLink(),
        width: 512,
        height: 512,
        colorDark : "#000000",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.M
    });
}

function getQrLink(){
    return location.origin + location.pathname + "?o=" + data.offerId
}

lobbifyRTC.appName = "FileFlick"
lobbifyRTC.serverDigest = "uukWyAxhmrC4JWjJpaZbBq2EFYsR46DJvBjEG0wu75ZM7UGz"
lobbifyRTC.useServerAuth = true
lobbifyRTC.useSSE = true

lobbifyRTC.httpRoot = "https://dagammla-ip.de/lobbify-rtc"
lobbifyRTC.wsRoot = "wss://dagammla-ip.de/lobbify-rtc"

/* *
lobbifyRTC.httpRoot = "http://localhost:8080"
lobbifyRTC.wsRoot = "ws://localhost:8080"
/* */

let lobby = null
let visitor = null

setTimeout(() => {
    const search = new URLSearchParams(location.search)
    const offerId = search.get("o")
    if (offerId != null && offerId.length > 0){
        data.isSending = false
        data.isInTransfer = true
        setTimeout(() => {
            caleJoin(offerId)
        }, 250)
    }
})


function serverPeerConfig(peer, visitorId){

    data.modalType = 'offer'

    validateClientId(visitorId).then((accepted) => {
        if (!accepted) {
            lobby.kick(visitorId, "rejected")
            data.modalType = "offer"
            return
        }

        const reliableChannel = peer.createDataChannel("file", {
            ordered: true
        })
        reliableChannel.binaryType = "arraybuffer"
        reliableChannel.onopen = () => {
    
            reliableChannel.send(fileToUpload.size + ":" + fileToUpload.name)
    
            const reader = new FileReader()
            reader.onload = () => {
                let offset = 0;
                
                function sendChunk() {
                    const chunk = reader.result.slice(offset, offset + chunkSize);
                    reliableChannel.send(chunk);
                    
                    offset += chunkSize;
        
                    if (offset < fileToUpload.size) {
                        // More chunks to send
                        setTimeout(sendChunk);
                    }
                }
        
                // Start sending the first chunk
                sendChunk();
            };
            reader.readAsArrayBuffer(fileToUpload);
    
            data.fileSize = fileToUpload.size
            data.modalType = "progress"
        }
    
        reliableChannel.onmessage = (ev) => {
            data.fileProgress = parseInt(ev.data + "")
            if (data.fileProgress >= data.fileSize){
                lobby.close()
                finished()
            }
        }
    
        reliableChannel.onclose = failed
    
        peer.onconnectionstatechange = () => {
            if (peer.connectionState == "closed"){
                failed()
            }
        }
    })
}

function clientPeerConfig(peer) {
    

    peer.ondatachannel = (ev) => {
        const receiveBuffer = [];
        const channel = ev.channel
        channel.binaryType = "arraybuffer"

        let hasReceivedInit = false

        channel.onmessage = (ev) => {

            if (!hasReceivedInit){
                const initMsg = ev.data + ""
                const splitPoint = initMsg.indexOf(":")
                data.fileSize = parseInt(initMsg.slice(0, splitPoint))
                data.fileName = initMsg.slice(splitPoint + 1)
                hasReceivedInit = true
                data.modalType = "progress"
                return
            }

            receiveBuffer.push(ev.data)
            data.fileProgress += ev.data.byteLength
            if (data.fileProgress >= data.fileSize){
                download(new Blob(receiveBuffer));
                setTimeout(finished)
            }
            channel.send("" + data.fileProgress)
            
        }
        channel.onclose = failed
    }
    

    peer.onconnectionstatechange = (ev) => {
        if (peer.connectionState == "closed"){
            failed()
        }
    }
}


var clientResolve = null
function acceptsClient(doesAccept){
    clientResolve.resolve(!!doesAccept)
}

function validateClientId(id){
    return new Promise((resolve) => {
        data.clientId = id
        clientResolve = resolve
        data.modalType = "confirm-pin"
    })
}

async function initHost(){
    lobby = await lobbifyRTC.createLobby(serverPeerConfig, failed, 1)
    data.offerId = lobby.lobbyId
    data.modalType = "offer"

    lobby.open()
}

async function caleJoin(offerId){
    visitor = await lobbifyRTC.visitLobby(offerId, failed)
    clientPeerConfig(visitor.peer)
    visitor.connect()
    data.clientId = visitor.visitorId;
    data.modalType = "confirm-pin"
}