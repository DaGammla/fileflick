# FileFlick

FileFlick is a web application that leverages WebRTC technology to facilitate fast and secure peer-to-peer file transfers between devices. With FileFlick, you can easily share files directly from your browser, without the need for cumbersome intermediary servers. The project is hosted using GitLab Pages, and you can access it at [https://dagammla.gitlab.io/fileflick](https://dagammla.gitlab.io/fileflick).

## Features

- **P2P File Transfer:** Transfer files directly between devices without relying on third-party servers.
- **WebRTC Technology:** Utilizes the power of WebRTC for secure and efficient real-time communication.
- **User-Friendly Interface:** Simple and intuitive interface for a seamless file-sharing experience.
- **Cross-Platform Compatibility:** Works on various devices and platforms that support modern web browsers.

## Getting Started

To use FileFlick, simply visit [https://dagammla.gitlab.io/fileflick](https://dagammla.gitlab.io/fileflick) in a WebRTC-compatible browser.

1. Open the FileFlick website.
2. Select the files you want to share.
3. Receive a unique sharing link.
4. Share the link with the recipient.
5. Once the recipient opens the link, the file transfer begins.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
